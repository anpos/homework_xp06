"""Maze solver."""

import math


class Vertex:
    """Vertex."""

    __slots__ = ['x', 'y']

    def __init__(self, x: int, y: int):
        """Init."""
        self.x, self.y = x, y

    def __repr__(self):
        """Repr."""
        return f"{self.yx}"

    def __eq__(self, other):
        """Equals."""
        return self.x == other.x and self.y == other.y

    def __lt__(self, other):
        """Less than."""
        return self.__hash__() < other.__hash__()

    def __hash__(self):
        """Hash."""
        return self.y << 16 + self.x

    def __add__(self, other):
        """Add."""
        return Vertex(self.x + other.x, self.y + other.y)

    @property
    def yx(self) -> tuple:
        """Tuple (y, x)."""
        return self.y, self.x

    def distance_axes(self, other):
        """Distance method 1."""
        return abs(self.x - other.x) + abs(self.y - other.y)

    def distance_squared(self, other):
        """Distance method 2."""
        xx, yy = self.x - other.x, self.y - other.y
        return xx * xx + yy * yy

    def distance_real(self, other):
        """Distance method 3."""
        return math.sqrt(self.distance_squared(other))


class Node:
    """Node of Dijkstra."""

    __slots__ = ['parent', 'vertex', 'g', 'h', 'f']

    def __init__(self, vertex: Vertex, parent=None, g=0):
        """Init."""
        self.parent = parent  # parent node
        self.vertex = vertex
        self.g = g  # node cost from the start
        self.h = 0  # estimated cost to the end
        self.f = 0  # node cost from the start + estimated cost to the end (priority)

    def __repr__(self):
        """Repr."""
        return f"Node(pos: {self.vertex}, g: {self.g}, h: {self.h}, f: {self.f})"

    def __hash__(self):
        """Hash."""
        return self.vertex.__hash__()

    def __eq__(self, other):
        """Equals."""
        return self.vertex == other.vertex

    def __lt__(self, other):
        """Less than."""
        return self.f < other.f

    def update_g(self, parent, g):
        """Set values."""
        self.parent = parent
        self.g = g
        self.f = g + self.h

    def update_gh(self, parent, g, h):
        """Set values."""
        self.parent = parent
        self.g = g
        self.h = h
        self.f = g + h

    def trace(self) -> list:
        """Trace nodes, return list of vertices."""
        path = []
        current_node = self
        if current_node.parent is not None:
            while current_node is not None:
                path.insert(0, current_node.vertex)
                current_node = current_node.parent
        return path


class Graph:
    """Graph."""

    def __init__(self, cost_table, directions):
        """Init."""
        self.cost_table = cost_table
        self.directions = directions

    def out_of_bounds(self, pos: Vertex) -> bool:
        """Check if vertex is in bounds."""
        return pos.y < 0 or pos.x < 0 or pos.y >= len(self.cost_table) or pos.x >= len(self.cost_table[pos.y])

    def cost(self, pos: Vertex) -> int:
        """Get vertex cost."""
        return self.cost_table[pos.y][pos.x]

    def impassable(self, pos: Vertex) -> bool:
        """Check if vertex is impassable."""
        return self.cost(pos) < 0

    def neighbors(self, of_pos: Vertex) -> list:
        """Get neighbors."""
        result = []
        for direction in self.directions:
            pos = of_pos + direction
            if self.out_of_bounds(pos) or self.impassable(pos):
                continue
            result.append((pos, self.cost(pos)))
        return result

    def vertices(self) -> list:
        """Get all vertices."""
        return [Vertex(x, y) for y in range(len(self.cost_table)) for x in range(len(self.cost_table[y]))]

    @staticmethod
    def heuristic(pos_from: Vertex, pos_to: Vertex):
        """Get estimated cost. Must be admissible but not consistent. Never overestimates the cost etc."""
        return pos_from.distance_axes(pos_to)


class SolverAlgorithm:
    """Path-finding algorithms."""

    def __init__(self, graph: Graph):
        """Init."""
        self.graph = graph

    def spfa(self, source: Vertex, destination: Vertex):
        """Bellman-Ford - Shortest Path Faster Algorithm."""
        node_map = {vertex: Node(vertex, None, float('inf')) for vertex in self.graph.vertices()}
        source_node = node_map[source]
        source_node.g = 0
        queue = [source_node]
        while len(queue) > 0:
            current_node = queue.pop(0)
            for neighbor, cost in self.graph.neighbors(current_node.vertex):
                neighbor_node = node_map[neighbor]
                g = current_node.g + cost
                if g < neighbor_node.g:
                    neighbor_node.update_g(current_node, g)
                    if neighbor_node not in queue:
                        queue.append(neighbor_node)
        result = node_map[destination].trace()
        if source != destination and result[0] == destination:
            return None, -1
        return result, node_map[destination].g

    def dijkstra(self, source: Vertex, destination: Vertex):
        """Dijkstra - with early exit."""
        node_map = {vertex: Node(vertex, None, float('inf')) for vertex in self.graph.vertices()}
        destination_node = node_map[destination]
        source_node = node_map[source]
        source_node.g = 0
        queue = [source_node]
        while len(queue) > 0:
            current_node = min(queue)
            queue.remove(current_node)
            if current_node == destination_node:
                return destination_node.trace(), destination_node.g
            for neighbor, cost in self.graph.neighbors(current_node.vertex):
                neighbor_node = node_map[neighbor]
                g = current_node.g + cost
                if g < neighbor_node.g:
                    neighbor_node.update_g(current_node, g)
                    if neighbor_node not in queue:
                        queue.append(neighbor_node)
        return None, -1


class MazeSolver:
    """Maze solver."""

    def __init__(self, maze_str: str, configuration: dict = None, door_symbol: str = '|', is_file: bool = False):
        """
        Initialize the solver with map string and configuration.

        Map string can consist of several lines, line break separates the lines.
        Empty lines in the beginning and in the end should be ignored.
        Line can also consist of spaces, so the lines cannot be stripped.
        Map can have non-rectangular shape (# indicates the border of the line):

        #####
        #   #
        #  #
        # #
        ##

        On the left and right sides there can be several doors (marked with "|").
        Solving the maze starts from a door from the left side and ends at the door on the right side.
        See more @solve().

        Configuration is a dict which indicates which symbols in the map string have what cost.
        Doors (|) are not shown in the configuration and are not used inside the maze.
        Door cell cost is 0.
        When a symbol on the map string is not in configuration, its cost is 0.
        Cells with negative cost cannot be moved on/through.

        Default configuration:
        configuration = {
            ' ': 1,
            '#': -1,
            '.': 2,
            '-': 5,
            'w': 10
        }

        :param maze_str: Map string
        :param configuration: Optional dictionary of symbol costs.
        """
        if is_file:
            with open(maze_str, 'r') as file:
                maze_str = file.read()

        if configuration is None:
            configuration = {
                ' ': 1,
                '#': -1,
                '.': 2,
                '-': 5,
                'w': 10
            }

        self.entries = []
        self.exits = []
        self.symbols = maze_str.strip('\n')

        cost_table = [[]]
        x, y = 0, 0
        for symbol in self.symbols:
            if symbol == '\n':
                x, y, = 0, y + 1
                cost_table.append([])
            elif symbol == door_symbol:
                cost = 0
                if x == 0:
                    self.entries.append(Vertex(x, y))
                else:
                    self.exits.append(Vertex(x, y))
                cost_table[y] += [cost]
                x += 1
            else:
                cost_table[y] += [configuration.get(symbol, 0)]
                x += 1
        self.graph = Graph(cost_table, [Vertex(0, -1), Vertex(0, 1), Vertex(-1, 0), Vertex(1, 0)])
        self.path_finder = SolverAlgorithm(self.graph)
        self.last_solution = None, -1

    def get_shortest_path(self, start: tuple, goal: tuple) -> tuple:
        """
        Return shortest path and the total cost of it.

        The shortest path is the path which has the lowest cost.
        Start and end are tuples of (y, x) where the first (upper) line is y = 0.
        The path should include both the start and the end.

        If there is no path from the start to goal, the path is None and cost is -1.

        If there are several paths with the same lowest cost, return any of those.

        :param start: Starting cell (y, x)
        :param goal: Goal cell (y, x)
        :return: shortest_path, cost
        """
        start, goal = Vertex(start[1], start[0]), Vertex(goal[1], goal[0])

        path, cost = self.path_finder.dijkstra(start, goal)
        if path is not None:
            path = [vertex.yx for vertex in path]  # make a list of ordinary tuples

        self.last_solution = path, cost
        return path, cost

    def solve(self) -> tuple:
        """
        Solve the given maze and return the path and the cost.

        Finds the shortest path from one of the doors on the left side to the one of the doors on the right side.
        Shortest path is the one with the lowest cost.

        This method should use get_shortest_path method and return the same values.
        If there are several paths with the same cost, return any of those.

        :return: shortest_path, cost
        """
        best_path = None
        best_cost = -1
        for entry_door in self.entries:
            for exit_door in self.exits:
                path, cost = self.get_shortest_path(entry_door.yx, exit_door.yx)
                if path is not None and (best_path is None or cost < best_cost or (cost == best_cost and len(path) < len(best_path))):
                    best_path = path
                    best_cost = cost
        self.last_solution = best_path, best_cost
        return best_path, best_cost

    def save_solved_maze(self, path_symbol: str, output_filename: str):
        """Save solved maze into a file."""
        with open(output_filename, 'w') as fp:
            fp.write(self.get_solved_maze(path_symbol))

    def get_solved_maze(self, path_symbol: str) -> str:
        """Get solved maze as a string."""
        path, cost = self.last_solution
        output = self.symbols
        lines = output.splitlines(True)
        if path is not None:
            for step in path:
                line = list(lines[step[0]])
                line[step[1]] = path_symbol
                lines[step[0]] = ''.join(line)
            output = ''.join(lines)
        return output


if __name__ == '__main__':
    maze = """
########
#      #
#      #
|      |
########
"""
    solver = MazeSolver(maze)
    assert solver.solve() == ([(3, 0), (3, 1), (3, 2), (3, 3), (3, 4), (3, 5), (3, 6), (3, 7)], 6)
    assert solver.get_shortest_path((3, 0), (3, 1)) == ([(3, 0), (3, 1)], 1)
    assert solver.get_shortest_path((3, 0), (2, 0)) == (None, -1)
    assert solver.get_shortest_path((3, 1), (3, 1)) == ([], 0)
    assert solver.get_shortest_path((3, 1), (3, 2))[1] == 1

    maze = """
#####
#   #
| # #
# # |
#####
"""
    solver = MazeSolver(maze)
    assert solver.solve() == ([(2, 0), (2, 1), (1, 1), (1, 2), (1, 3), (2, 3), (3, 3), (3, 4)], 6)

    maze = """
#####
#   |
#   |
| # #
#####
| # |
#####
"""
    solver = MazeSolver(maze)
    assert solver.solve() == ([(3, 0), (3, 1), (2, 1), (2, 2), (2, 3), (2, 4)], 4)
    # multiple paths possible, let's just assert the cost
    assert solver.get_shortest_path((3, 0), (1, 4))[1] == 4  # using the door at (2, 4)
    assert solver.get_shortest_path((5, 0), (5, 4)) == (None, -1)

    configuration = {' ': 1, '+': -1, '-': -1, '|': -1, '#': -1}

    solver = MazeSolver("./maze1.txt", configuration, 'X', True)
    solver.solve()
    solver.save_solved_maze('o', "./solved_maze1.txt")

    solver = MazeSolver("./maze2.txt", configuration, 'X', True)
    solver.solve()
    solver.save_solved_maze('o', "./solved_maze2.txt")

    solver = MazeSolver("./maze3.txt", None, '|', True)
    solver.solve()
    solver.save_solved_maze('o', "./solved_maze3.txt")

    solver = MazeSolver("./maze4.txt", None, '|', True)
    solver.solve()
    solver.save_solved_maze('o', "./solved_maze4.txt")

    solver = MazeSolver("./maze5.txt", None, '|', True)
    solver.solve()
    solver.save_solved_maze('o', "./solved_maze5.txt")
